﻿# The script of the game goes in this file.


# Declare characters used by this game. The color argument colorizes the
# name of the character.

define e = Character("Eileen")


# The game starts here.

label start:

    # Show a background. This uses a placeholder by default, but you can
    # add a file (named either "bg room.png" or "bg room.jpg") to the
    # images directory to show it.

    scene university

    # This shows a character sprite. A placeholder is used, but you can
    # replace it by adding a file named "eileen happy.png" to the images
    # directory.

    show james

    # These display lines of dialogue.

    "James" "Welcome to JAR University!"

    scene roundabout
    hide James
    
    show randy at right
    show crafting-jim at left
    "Randy" "THIS ENDS NOW!!!!!!!!!!!" 
    # This ends the game.
    hide randy
    hide crafting-jim
    scene alley
    show fat-detective at left
    show crafting-alex at right
    "Fat Detective" "No! It cannot be so!"

    scene uni_hall
    hide fat-detective
    hide crafting-alex
    show dobby-uni at center
    "It is time to be sorted my little jarling"
    "I deem you to be..."
    house  = renpy.random.randint(1, 4)
    if house == 1:
        "You have been sorted into House Beltman!"
    if house == 2:
        "You have been sorted into House House!"
    if house == 3:
        "You have been sorted into House Dibby!"
    return
